# apache-graphql

GraphQL interface to Apache Projects catalog

## Usage

      docker run -p 5000:5000 mswinson/apache-graphql

## Configuration

      Ports
      - 5000


### Development

## Usage

run

    export LOCALHOST=<yourhost>
    docker-compose up

open

    http://LOCALHOST:4000


### Contributing

1. Fork it ( http://bitbucket.org/mswinson-planetdb/apache-graphql/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request

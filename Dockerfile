FROM ruby:latest

ADD . /var/services/apache-graphql
WORKDIR /var/services/apache-graphql

RUN ./scripts/setup
CMD ./scripts/init
